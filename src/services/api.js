import * as carOfTheWeek from './__fixtures__/carOfTheWeek';
import { data as makes } from './__fixtures__/makes';
import { data as models } from './__fixtures__/models';

export const getCarOfTheWeek = () => carOfTheWeek;
export const getMakes = () => makes;
export const getModels = () => models;
