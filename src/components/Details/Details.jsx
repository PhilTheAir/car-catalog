import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'grid-styled';
import { withRouter } from 'react-router-dom';
import { get } from 'lodash';

class Details extends Component {
  static propTypes = {
    model: PropTypes.shape({
      id: PropTypes.number,
      imageUrl: PropTypes.string,
      make: PropTypes.string,
      makeId: PropTypes.number,
      price: PropTypes.number,
      name: PropTypes.string
    }).isRequired,
    requestCarModels: PropTypes.func.isRequired,
    requestCarMakes: PropTypes.func.isRequired,
  };

  static defaultProps = {
    model: {},
    requestCarModels: () => { },
    requestCarMakes: () => { },
  };

  componentDidMount() {
    const { requestCarModels, requestCarMakes } = this.props;
    if (requestCarModels && requestCarMakes) {
      requestCarModels();
      requestCarMakes()
    }
  }

  render() {
    const modelId = get(this.props, 'match.params.id', '');
    const modelObj = get(this.props, `model.byId.${modelId}`, {});
    const { make, name, price, imageUrl } = modelObj;
    return (
      <Flex flexDirection="column">
        {
          name ? (
            <ul>
              <li>{make}</li>
              <li>{name}</li>
              <li>{price}</li>
            </ul>
          ) : null
        }
        {
          imageUrl ? (
            <img src={imageUrl} alt={name} width="300" />
          ) : null
        }
      </Flex>
    );
  }
}

export default withRouter(Details);