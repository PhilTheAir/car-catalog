import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex } from 'grid-styled';

export default class WeeklyCar extends Component {
  static propTypes = {
    car: PropTypes.shape({
      name: PropTypes.string,
      price: PropTypes.number,
      imageUrl: PropTypes.string,
      review: PropTypes.string
    }).isRequired,
    requestWeeklyCar: PropTypes.func.isRequired,
    requestCarModels: PropTypes.func.isRequired,
    requestCarMakes: PropTypes.func.isRequired,
  };

  static defaultProps = {
    car: {},
    requestWeeklyCar: () => { },
    requestCarModels: () => { },
    requestCarMakes: () => { },
  };

  componentDidMount() {
    const { requestWeeklyCar, requestCarModels, requestCarMakes } = this.props;
    if (requestWeeklyCar && requestCarModels && requestCarMakes) {
      requestWeeklyCar();
      requestCarModels();
      requestCarMakes()
    }
  }

  render() {
    const { make, name, price, imageUrl, review } = this.props.car;
    return (
      <Flex flexDirection="column">
        {
          name ? (
            <ul>
              <li>{make}</li>
              <li>{name}</li>
              <li>{price}</li>
              <li>{review}</li>
            </ul>
          ) : null
        }
        {
          review ? (
            <img src={imageUrl} alt={name} width="300" />
          ) : null
        }
      </Flex>
    );
  }
}
