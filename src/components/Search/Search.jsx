import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Flex, Box } from 'grid-styled';
import styled from 'styled-components';
import { withRouter } from 'react-router-dom';

const ColumnBox = styled(Box)`
  background-color: #007DBA;
  text-align: center;
  border-radius: 3px;
  border: none;
  min-height: 100px;
  margin-bottom: 20px;
`;

const ItemBox = styled(Box)`
  background-color: #007DBA;
  cursor: pointer;
  border: none;
  width: 100%;
  height: 30px;
  color: white;
  font-size: 14px;
  padding-top: 6px;
  &:hover {
    background-color: #008DD4;
    color: black;
    font-size: 18px;
  }
`;

const ButtonBox = styled(Box)`
  background-color: ${props => props.type ? '#007DBA' : 'gray'};
  text-align: center;
  border-radius: 3px;
  font-size: 18px;
  cursor: ${props => props.type ? 'pointer' : 'not-allowed'};
  border: 1px solid gray;
  height: 36px;
  color: white;
  pointer-events: ${props => props.type ? 'initial' : 'none'};
`;

class Search extends Component {
  static propTypes = {
    makes: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        id: PropTypes.number,
      })).isRequired,
    models: PropTypes.arrayOf(
      PropTypes.shape({
        imageUrl: PropTypes.string,
        id: PropTypes.number,
        make: PropTypes.string,
        makeId: PropTypes.number,
        name: PropTypes.string,
        price: PropTypes.number
      })).isRequired,
    requestCarModels: PropTypes.func.isRequired,
    requestCarMakes: PropTypes.func.isRequired,
  };

  static defaultProps = {
    makes: [],
    models: [],
    requestCarModels: () => { },
    requestCarMakes: () => { },
  };

  state = {
    make: '',
    makeId: '',
    modelId: '',
  }

  componentDidMount() {
    const { requestCarModels, requestCarMakes } = this.props;
    if (requestCarModels && requestCarMakes) {
      requestCarModels();
      requestCarMakes()
    }
  }

  handleMakeClick = (make, makeId) => {
    this.setState({
      make,
      makeId
    })
  }

  handleModelClick = modelId => {
    this.setState({
      modelId
    })
  }

  handleButtonClick = link => {
    this.props.history.push(`make/model/${this.state.modelId}`);
  }

  render() {
    const { makes, models } = this.props;
    return (
      <Flex flexDirection={"column"} justifyContent="center" alignItems="center">
        <Flex flexDirection={["column", "row"]}>
          <ColumnBox w={[1, 300]} mr={[0, 20]}>
            {
              Array.isArray(makes) && makes.map(make => (
                <ItemBox
                  is="button"
                  role="link"
                  aria-label={make.name}
                  tabIndex={0}
                  key={`make${make.id}`}
                  onClick={() => this.handleMakeClick(make.name, make.id)}>
                  {make.name}
                </ItemBox>
              ))
            }
          </ColumnBox>
          <ColumnBox w={[1, 300]}>
            {
              this.state.make && Array.isArray(models) && models.filter(model => model.makeId === this.state.makeId).map(model => (
                <ItemBox
                  is="button"
                  role="link"
                  aria-label={model.name}
                  tabIndex={0}
                  key={`model${model.id}`}
                  onClick={() => this.handleModelClick(model.id)}>
                  {`${this.state.make} ${model.name}`}
                </ItemBox>
              ))
            }
          </ColumnBox>
        </Flex>
        <ButtonBox
          is="button"
          role="link"
          aria-label='seedetails'
          tabIndex={0} type={this.state.modelId}
          onClick={() => this.handleButtonClick()}
          w={[1, 300]}>
          See details
        </ButtonBox>
      </Flex>
    );
  }
}

export default withRouter(Search);