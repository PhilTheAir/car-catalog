import React from 'react';
import { Footer as RawFooter } from '../Layout';

const Footer = () => (
  <RawFooter>
    <p>Auther: Liwen CHEN</p>
  </RawFooter>
)
export default Footer;