import React from 'react';
import { Header as RawHeader } from '../Layout';

const Header = () => (
  <RawHeader>
    <h1>Car Catalog</h1>
  </RawHeader>
)
export default Header;