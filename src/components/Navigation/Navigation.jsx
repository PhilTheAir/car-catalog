import React, { Component } from 'react';
import PropTypes from 'prop-types';
import * as Ionicons from 'react-icons/lib/io';
import { Nav } from '../Layout';
import { LinkIcon } from '../LinkIcon';

export default class Navigation extends Component {
  static propTypes = {
    links: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        href: PropTypes.string.isRequired,
        icon: PropTypes.object.isRequired
      }).isRequired,
    ).isRequired
  };

  static defaultProps = {
    links: [
      {
        "title": "Home",
        "href": "/",
        "icon": <Ionicons.IoHome />
      },
      {
        "title": "Search",
        "href": "/search",
        "icon": <Ionicons.IoSearch />
      }
    ]
  };

  render() {
    const { links } = this.props;
    return (
      <Nav ariaLabelledby="mainnavheader" title="Navigation Menu">
        {
          links.map(link => <LinkIcon
            key={link.title}
            title={link.title}
            link={link.href}
            icon={link.icon}
          />)
        }
      </Nav>
    )
  }
}