import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import HomeLayout from './layouts/HomeLayout';
import SearchLayout from './layouts/SearchLayout';
import DetailsLayout from './layouts/DetailsLayout';

const Routes = () => (
  <Router>
    <Switch>
      <Route path="/search" component={SearchLayout} />
      <Route path="/make/model/:id" component={DetailsLayout} />
      <Route path="/" component={HomeLayout} />
    </Switch>
  </Router>
);

export default Routes;
