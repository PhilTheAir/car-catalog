import React from 'react';
import { PageWrapper, Page, Main } from '../components/Layout';
import { Header } from '../components/Header';
import { Navigation } from '../components/Navigation';
import { Details } from '../components/Details';
import { Footer } from '../components/Footer';
import { connect } from 'react-redux';
import { getCarModelsSlice } from '../redux/selectors';
import { actions as carModelsActions } from '../redux/modules/appcarmodels.module';
import { actions as carMakesActions } from '../redux/modules/appcarmakes.module';

const DetailsContainer = ({ model, requestCarModels, requestCarMakes }) => (
  <PageWrapper>
    <Page>
      <Header />
      <Navigation />
      <Main>
        <Details model={model} requestCarModels={requestCarModels} requestCarMakes={requestCarMakes} />
      </Main>
      <Footer />
    </Page>
  </PageWrapper>
);

export default connect(
  state => ({
    model: getCarModelsSlice(state),
  }),
  {
    requestCarModels: carModelsActions.app.carmodels.request,
    requestCarMakes: carMakesActions.app.carmakes.request,
  }
)(DetailsContainer);