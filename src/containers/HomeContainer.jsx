import React from 'react';
import { PageWrapper, Page, Main } from '../components/Layout';
import { Header } from '../components/Header';
import { Navigation } from '../components/Navigation';
import { WeeklyCar } from '../components/WeeklyCar';
import { Footer } from '../components/Footer';
import { connect } from 'react-redux';
import { getWeeklyCarList } from '../redux/selectors';
import { actions as weeklyCarActions } from '../redux/modules/appweeklycar.module';
import { actions as carMakesActions } from '../redux/modules/appcarmakes.module';
import { actions as carModelsActions } from '../redux/modules/appcarmodels.module';

const HomeContainer = ({ car, requestWeeklyCar, requestCarModels, requestCarMakes }) => (
  <PageWrapper>
    <Page>
      <Header />
      <Navigation />
      <Main>
        <WeeklyCar car={car} requestWeeklyCar={requestWeeklyCar} requestCarModels={requestCarModels} requestCarMakes={requestCarMakes} />
      </Main>
      <Footer />
    </Page>
  </PageWrapper>
);

export default connect(
  state => ({
    car: getWeeklyCarList(state),
  }),
  {
    requestWeeklyCar: weeklyCarActions.app.weeklycar.request,
    requestCarModels: carModelsActions.app.carmodels.request,
    requestCarMakes: carMakesActions.app.carmakes.request,
  }
)(HomeContainer);
