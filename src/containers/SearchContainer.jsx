import React from 'react';
import { PageWrapper, Page, Main } from '../components/Layout';
import { Header } from '../components/Header';
import { Footer } from '../components/Footer';
import { Navigation } from '../components/Navigation';
import { Search } from '../components/Search';
import { connect } from 'react-redux';
import { getCarMakesList, getCarModelsList } from '../redux/selectors';
import { actions as carModelsActions } from '../redux/modules/appcarmodels.module';
import { actions as carMakesActions } from '../redux/modules/appcarmakes.module';

const SearchContainer = ({ makes, models, requestCarMakes, requestCarModels }) => (
  <PageWrapper>
    <Page>
      <Header />
      <Navigation />
      <Main>
        <Search makes={makes} models={models} requestCarMakes={requestCarMakes} requestCarModels={requestCarModels} />
      </Main>
      <Footer />
    </Page>
  </PageWrapper>
);

export default connect(
  state => ({
    makes: getCarMakesList(state),
    models: getCarModelsList(state),
  }),
  {
    requestCarModels: carModelsActions.app.carmodels.request,
    requestCarMakes: carMakesActions.app.carmakes.request,
  }
)(SearchContainer);
