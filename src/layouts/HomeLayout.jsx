import React from 'react';
import HomeContainer from '../containers/HomeContainer';

const HomeLayout = props => (
  <HomeContainer />
);

export default HomeLayout;