import React from 'react';
import SearchContainer from '../containers/SearchContainer';

const SearchLayout = props => (
  <SearchContainer />
);

export default SearchLayout;