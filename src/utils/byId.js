export const getValueById = objArr => {
  let result = {
    allIds: [],
    byId: {}
  };
  objArr.forEach(obj => {
    const id = obj.id;
    result.allIds.push(id);
    result.byId[id] = obj;
  });
  return result;
}