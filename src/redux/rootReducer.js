import { combineReducers } from 'redux';
import weeklyCarReducer from './modules/appweeklycar.module';
import carMakesReducer from './modules/appcarmakes.module';
import carModelsReducer from './modules/appcarmodels.module';

const rootReducer = combineReducers({
  entities: combineReducers({
    weeklyCar: weeklyCarReducer,
    carMakes: carMakesReducer,
    carModels: carModelsReducer,
  })
});

export default rootReducer;
