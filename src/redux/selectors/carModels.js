import { get } from 'lodash';
import { createSelector } from 'reselect';
import { getCarMakesSlice } from './index';
import { getValueById } from '../../utils'

export const getCarModelsListRaw = state => get(state, 'entities.carModels', null);

export const getCarModelsSlice = createSelector(
  [getCarModelsListRaw, getCarMakesSlice],
  (carModels, carMakes) => {
    if (!carModels || !Array.isArray(carModels) || !carMakes) {
      return {};
    }
    const carModelsMakes = carModels.map(model => ({
      ...model,
      make: carMakes.byId[model.makeId].name
    }))
    const carModelsSlice = getValueById(carModelsMakes);
    return carModelsSlice;
  }
);

export const getCarModelsList = createSelector(
  [getCarModelsSlice],
  (carModels) => {
    if (!carModels || (carModels.allIds === undefined)) {
      return [];
    }
    const { allIds, byId } = carModels;
    const result = allIds.map(id => byId[id]);
    return result;
  }
);
