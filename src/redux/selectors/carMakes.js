import { get } from 'lodash';
import { createSelector } from 'reselect';
import { getValueById } from '../../utils';

export const getCarMakesList = state => get(state, 'entities.carMakes', null);

export const getCarMakesSlice = createSelector(
  [getCarMakesList],
  carMakes => {
    if (!carMakes || !Array.isArray(carMakes)) {
      return [];
    }
    const carMakesSlice = getValueById(carMakes);
    return carMakesSlice;
  }
);
