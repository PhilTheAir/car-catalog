import { get } from 'lodash';
import { createSelector } from 'reselect';
import { getCarModelsSlice } from './index';

export const getWeeklyCarListRaw = state => get(state, 'entities.weeklyCar', null);

export const getWeeklyCarList = createSelector(
  [getCarModelsSlice, getWeeklyCarListRaw],
  (carModels, weeklyCarListRaw) => {
    if (!carModels || (carModels.allIds === undefined) || !weeklyCarListRaw) {
      return {};
    }
    const { modelId } = weeklyCarListRaw;
    const model = carModels.byId[modelId];
    const result = {
      ...weeklyCarListRaw,
      ...model
    }
    return result;
  }
);
