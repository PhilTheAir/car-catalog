import { put, call, takeEvery } from 'redux-saga/effects';
import { actions } from '../modules/appcarmakes.module';
import { getMakes } from '../../services/api';

export function* watchAppCarMakes() {
  yield takeEvery(actions.app.carmakes.request().type, loadAppCarMakes);
}

export function* loadAppCarMakes() {
  try {
    const carmakes = yield call(getMakes);
    yield put(actions.app.carmakes.receive(carmakes));
  }
  catch (error) {
    yield put(actions.app.carmakes.error(error));
  }
}
