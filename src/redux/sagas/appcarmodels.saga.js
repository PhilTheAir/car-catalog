import { put, call, takeEvery } from 'redux-saga/effects';
import { actions } from '../modules/appcarmodels.module';
import { getModels } from '../../services/api';

export function* watchAppCarModels() {
  yield takeEvery(actions.app.carmodels.request().type, loadAppCarModels);
}

export function* loadAppCarModels() {
  try {
    const carmodels = yield call(getModels);
    yield put(actions.app.carmodels.receive(carmodels));
  }
  catch (error) {
    yield put(actions.app.carmodels.error(error));
  }
}
