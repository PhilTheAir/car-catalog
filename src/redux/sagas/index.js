import { all } from 'redux-saga/effects';
import { watchAppWeeklyCar } from './appweeklycar.saga';
import { watchAppCarMakes } from './appcarmakes.saga';
import { watchAppCarModels } from './appcarmodels.saga';

export default function* rootSaga() {
  yield all([
    watchAppWeeklyCar(),
    watchAppCarMakes(),
    watchAppCarModels(),
  ]);
}