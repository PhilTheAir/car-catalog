import { put, call, takeEvery } from 'redux-saga/effects';
import { actions } from '../modules/appweeklycar.module';
import { getCarOfTheWeek } from '../../services/api';

export function* watchAppWeeklyCar() {
  yield takeEvery(actions.app.weeklycar.request().type, loadAppWeeklyCar);
}

export function* loadAppWeeklyCar() {
  try {
    const weeklycar = yield call(getCarOfTheWeek);
    yield put(actions.app.weeklycar.receive(weeklycar));
  }
  catch (error) {
    yield put(actions.app.weeklycar.error(error));
  }
}
