import { createActions, handleActions } from 'redux-actions';

export const actions = createActions({
  app: {
    carmakes: {
      REQUEST: () => { },
      RECEIVE: carmakes => carmakes,
      ERROR: null,
    },
  },
});

export const reducer = handleActions(
  {
    [actions.app.carmakes.request]: (state, action) => ({ ...state }),
    [actions.app.carmakes.receive]: (state, action) => action.payload,
    [actions.app.carmakes.error]: (state, action) => ({}),
  },
  {}
);

export default reducer;