import { createActions, handleActions } from 'redux-actions';

export const actions = createActions({
  app: {
    carmodels: {
      REQUEST: () => { },
      RECEIVE: carmodels => carmodels,
      ERROR: null,
    },
  },
});

export const reducer = handleActions(
  {
    [actions.app.carmodels.request]: (state, action) => ({ ...state }),
    [actions.app.carmodels.receive]: (state, action) => action.payload,
    [actions.app.carmodels.error]: (state, action) => ({}),
  },
  {}
);

export default reducer;