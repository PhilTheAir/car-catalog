import { createActions, handleActions } from 'redux-actions';

export const actions = createActions({
  app: {
    weeklycar: {
      REQUEST: () => { },
      RECEIVE: weeklycar => ({ ...weeklycar }),
      ERROR: null,
    },
  },
});

export const reducer = handleActions(
  {
    [actions.app.weeklycar.request]: (state, action) => ({ ...state }),
    [actions.app.weeklycar.receive]: (state, action) => ({
      ...state,
      ...action.payload,
    }),
    [actions.app.weeklycar.error]: (state, action) => ({}),
  },
  {}
);

export default reducer;